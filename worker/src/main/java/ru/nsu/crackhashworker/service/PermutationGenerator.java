package ru.nsu.crackhashworker.service;

/*
 * Сначала вычисляется общее число перестановок, которые могут быть сгенерированы
 * на основе alphabet и length. Затем определяется размер каждой части перестановок,
 * который будет генерироваться каждым работником, и вычисляетсЯ начальный индекс
 * для генерации перестановок на основе номера части и общего числа частей.
 * Затем функция перебирает индексы для генерации перестановок и для каждого индекса
 * генерирует соответствующую перестановку.
 *
 * Объект PermutationGenerator хранит состояние генерации перестановок,
 * включая текущий индекс перестановки. При каждом вызове метода next()
 * происходит генерация следующей перестановки. Если перестановки закончились,
 * метод возвращает null.
 *
 * Пример вызова:
 * PermutationGenerator generator = new PermutationGenerator(alphabet, length, partNum, countNum);
 * String permutation;
 * while ((permutation = generator.next()) != null) { System.out.println(permutation); }
 */

import java.util.List;

public class PermutationGenerator {
    private final String[] alphabet;
    private final int length;
    private final int totalPermutations;
    private final int partSize;
    private final int startIndex;
    private int currentPermutationIndex;

    public PermutationGenerator(String[] alphabet, int length, int partNum, int countNum) {
        this.alphabet = alphabet;
        this.length = length;
        this.totalPermutations = (int) Math.pow(alphabet.length, length);
        this.partSize = (int) Math.ceil((double) totalPermutations / countNum);
        this.startIndex = partNum * partSize;
        this.currentPermutationIndex = startIndex;
    }

    public String next() {
        if (currentPermutationIndex >= startIndex + partSize || currentPermutationIndex >= totalPermutations) {
            return null;
        }

        String permutation = "";
        int remainder = currentPermutationIndex;
        for (int j = 0; j < length; j++) {
            permutation += alphabet[remainder % alphabet.length];
            remainder /= alphabet.length;
        }

        currentPermutationIndex++;
        return permutation;
    }
}
