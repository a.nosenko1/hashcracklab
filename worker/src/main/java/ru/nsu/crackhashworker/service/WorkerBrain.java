package ru.nsu.crackhashworker.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.nsu.crackhashworker.model.CrackHashManagerRequest;
import ru.nsu.crackhashworker.model.CrackHashWorkerResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


@Service
public class WorkerBrain {
    static final Logger log = LoggerFactory.getLogger(WorkerBrain.class);

    private final String managerUrl;

    public WorkerBrain(@Value("${MANAGER_URL}") String managerUrl){
        this.managerUrl = managerUrl;
    }

    @Async
    public void calc(String requestId, int partNum, int partCount, String hash, int length,
                            CrackHashManagerRequest.Alphabet alphabet) throws NoSuchAlgorithmException {

        String[] symbols = alphabet.getSymbols().toArray(new String[0]);

        CrackHashWorkerResponse.Answers answers = new CrackHashWorkerResponse.Answers();

        PermutationGenerator generator = new PermutationGenerator(symbols, length, partNum, partCount);
        String permutation;

        log.info("Starting permutation generator RequestId = " + requestId + " partNum = " + partNum);
        while ((permutation = generator.next()) != null) {
            if (calcHash(permutation).equals(hash)){
                answers.getWords().add(permutation);
            }
        }
        log.info("Words found " + answers.getWords().size());
        log.info("Words : " + answers.getWords());

        sendResponse(requestId, partNum, answers);
    }

    public void sendResponse(String requestId, int part, CrackHashWorkerResponse.Answers answers){
        RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());

        try{
            CrackHashWorkerResponse response = new CrackHashWorkerResponse();

            response.setRequestId(requestId);
            response.setPartNumber(part);
            response.setAnswers(answers);

            HttpEntity<CrackHashWorkerResponse> request1 = new HttpEntity<>(response);
            log.info("response with RequestId = " + request1.getBody().getRequestId() + " formed");

            restTemplate.patchForObject(managerUrl, request1, CrackHashWorkerResponse.class);
            log.info("Sent PATCH request with RequestId = " + request1.getBody().getRequestId());
        }
        catch (Exception e){
            log.error("Exception while sending Response: " + e);
        }
    }

    public static String calcHash(String input) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(input.getBytes());
        byte[] digest = md.digest();

        StringBuilder hexString = new StringBuilder();
        for (byte b : digest) {
            hexString.append(String.format("%02x", b & 0xff));
        }

        return hexString.toString();
    }

}
