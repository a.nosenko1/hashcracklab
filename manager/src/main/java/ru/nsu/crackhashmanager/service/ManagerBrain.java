package ru.nsu.crackhashmanager.service;


import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.nsu.crackhashmanager.model.CrackHashManagerRequest;
import ru.nsu.crackhashmanager.model.CrackTask;
import ru.nsu.crackhashmanager.model.ObjectFactory;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ManagerBrain {

    private final String workerHost;
    private final String workerApi;
    private final int workersCount;
    private final String[] portRange;
    static final Logger log = LoggerFactory.getLogger(ManagerBrain.class);

    char[] symbols = "abcdefghijklmnopqrstuvwxyz1234567890".toCharArray();

    public ManagerBrain(@Value("${WORKER_HOST}") String workerHost,
                        @Value("${WORKER_API}") String workerApi,
                        @Value("${WORKERS_COUNT}") int workersCount,
                        @Value("${WORKERS_PORT_RANGE}") String portRangeEnv){
        this.workerHost = workerHost;
        this.workerApi = workerApi;
        this.workersCount = workersCount;

        for (char s: symbols) {
            alphabet.getSymbols().add(String.valueOf(s));
        }
        portRange = parseRange(portRangeEnv);
    }

    CrackHashManagerRequest.Alphabet alphabet = new CrackHashManagerRequest.Alphabet();
    ConcurrentHashMap<String, CrackTask> tasksMap = new ConcurrentHashMap<String, CrackTask>();

    public String createNewCrackTask(String hash, int maxLength){

        log.info("Creating a new task: hash = " + hash);

        String requestID = java.util.UUID.randomUUID().toString();
        CrackTask crackTask = new CrackTask(requestID, hash, maxLength, CrackTask.TaskStatus.IN_PROGRESS, workersCount);
        tasksMap.put(requestID, crackTask);

        try{
            for (int i = 0; i < workersCount; i++) {
                postTask(requestID, hash, maxLength, i, workersCount);
            }
        }
        catch (Exception e){
            log.error("request to worker not sent: " + e);
            tasksMap.get(requestID).setStatus(CrackTask.TaskStatus.ERROR);
        }

        return requestID;
    }

    public String getTaskStatus(String requestID){
        return tasksMap.get(requestID).getTaskStatus().toString();
    }
    public String[] getTaskData(String requestID){
        String[] data = tasksMap.get(requestID).getData();
        if (data.length > 0)
            return data;
        return null;
    }

    private void postTask(String requestID, String hash, int maxLength, int partNumber, int partCount){

        RestTemplate restTemplate = new RestTemplate();

        ObjectFactory objectFactory = new ObjectFactory();
        CrackHashManagerRequest request = objectFactory.createCrackHashManagerRequest();

        request.setRequestId(requestID);
        request.setHash(hash);
        request.setAlphabet(alphabet);
        request.setMaxLength(maxLength);
        request.setPartNumber(partNumber);
        request.setPartCount(partCount);

        String url = workerHost + ":" + portRange[partNumber] + workerApi;

        HttpEntity<CrackHashManagerRequest> request1 = new HttpEntity<>(request);
        log.info("Request with RequestId = " + request1.getBody().getRequestId() + " formed");

        restTemplate.postForObject(url, request1, CrackHashManagerRequest.class);
        log.info("Sent post request with RequestId = " + request1.getBody().getRequestId() +
                "; partNumber=" + partNumber);
    }

    public static String[] parseRange(String range) {
        String[] parts = range.split("-");
        int start = Integer.parseInt(parts[0]);
        int end = Integer.parseInt(parts[1]);
        int size = end - start + 1;
        String[] result = new String[size];
        for (int i = 0; i < size; i++) {
            result[i] = Integer.toString(start + i);
        }
        return result;
    }

    public void receiveWorkerResponse(String requestID, List<String> words){
        tasksMap.get(requestID).addWords(words);
    }

    @Scheduled(fixedRate = 2000)
    public void checkTimeout() {
        for (var entry : tasksMap.entrySet()) {
            if (entry.getValue().checkSecondsOld() > 10
                    && entry.getValue().getTaskStatus() == CrackTask.TaskStatus.IN_PROGRESS){
                entry.getValue().setStatus(CrackTask.TaskStatus.ERROR);
                log.info("Timeout error: " + entry.getValue().getRequestID());
            }
        }
        log.info("Timeout checked");
    }

}
