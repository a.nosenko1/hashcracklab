package ru.nsu.crackhashmanager.model;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class CrackRequest {
    private String hash;
    private int maxLength;
}
