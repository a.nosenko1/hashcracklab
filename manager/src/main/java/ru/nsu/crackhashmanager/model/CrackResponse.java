package ru.nsu.crackhashmanager.model;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class CrackResponse {
    public CrackResponse(String requestId){
        this.requestId = requestId;
    }
    private String requestId;
}
